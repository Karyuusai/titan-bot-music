const { Schema, model } = require('mongoose')

const guildSchema = new Schema({
    _id: String,
    setup: {
        channel_id: String,
        queue_id: String,
        now_play_id: String,
        in_voice: Boolean
    }
    
})

module.exports = model('guilds', guildSchema)