const Event = require('../../structures/Event')

module.exports = class extends Event {
    constructor(client){
        super(client, {
            name:'ready'
        })
    }

    run = async () => {
        console.log(`Bot ${this.client.user.username} logged in ${this.client.guilds.cache.size} guilds`)
        this.client.registryCommands()
        await this.client.connectToDatabase()

        this.client.manager.init(this.client.user.id)
    }
}