const Event = require('../../structures/Event')
const Queue = require('../../util/queue/queue')

module.exports = class extends Event {
    constructor(client){
        super(client, {
            name:'messageReactionAdd'
        })
    }

    run = async(messageReaction, user) => {

        if (user.id === this.client.user.id) return

        const db = await this.client.db.guilds.findById(messageReaction.message.guildId)

        const member = this.client.guilds.cache.get(messageReaction.message.guildId).members.cache.get(user.id)
        
        if (messageReaction.message.channelId !== db.setup.channel_id) return
        if (messageReaction.message.id !== db.setup.now_play_id && messageReaction.message.id !== db.setup.queue_id) return
        
        messageReaction.message.reactions.resolve(`${messageReaction._emoji.name}`).users.remove(user)

        const player = this.client.manager.get(messageReaction.message.guildId)
        if (!player) return

        if (member.voice.channel.id !== player.voiceChannel) return

        switch(messageReaction._emoji.name) {

            case '⏭️':
                player.stop()
                break
            
            case '⏹️':
                player.destroy()
                break
                
            case '⏯️':
                player.pause(!player.paused)
                break

            case '🔀':
                player.queue.shuffle()
                const queue = new Queue(this.client)
                queue.changeQueue(player)
                break

            default:
                return
        }
    }
}