const Event = require('../../structures/Event')
const Play = require('../../util/play')

module.exports = class extends Event {
    constructor(client) {
        super(client, {
            name: 'messageCreate'
        })
    }

    run = async (message) => {
        if (message.author.id === this.client.user.id) return
        const db = await this.client.db.guilds.findById(message.guild.id)
        
        if (message.channel.id === db.setup.channel_id) {

            message.delete()

            if (!message.member.voice.channel) return message.reply({ content: 'canal de voz', ephemeral: true })
            if (message.guild.me.voice.channel && message.guild.me.voice.channel.id !== message.member.voice.channel.id) return 

            const play = new Play(this.client, message)
            play.run()
            
            return
        }
    }
}
