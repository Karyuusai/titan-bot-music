const Command = require('../../structures/Command')


module.exports = class extends Command {
    constructor(client) {
        super(client, {
            name: 'test',
            description: 'bot ping'
        })
    }
    run = async(interaction) => {
        const guild = interaction.guild
        console.log(guild.me.voice.channel)
    }
}
