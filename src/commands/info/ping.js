const Command = require('../../structures/Command')


module.exports = class extends Command {
    constructor(client) {
        super(client, {
            name: 'ping',
            description: 'bot ping'
        })
    }
    run = (interaction) => {
        interaction.reply({
            content: `The bot ping is \`${this.client.ws.ping}\`ms`,
            ephemeral: 'true'
        })
    }
}
