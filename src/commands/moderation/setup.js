const { MessageEmbed, Message } = require('discord.js')
const Command = require('../../structures/Command')

emojis = ['⏯️', '⏹️', '⏭️', '🔄', '🔀', '❌']

module.exports = class extends Command {
    constructor(client) {
        super(client, {
            name: 'setup',
            description: 'setup in a channel',
            requireDatabase: true
        })
    }

    run = async (interaction) => {
        const channel = await interaction.guild.channels.create('titan-channel', { type: 'text' })         

        const queueEmbed = new MessageEmbed()
            .setTitle('__Queue list__')
            .setDescription('Join a voice channel and queue songs by name or url in here.')
            .setColor('ORANGE')

        const nowPlayingEmbed = new MessageEmbed()
            .setTitle('No song playing currently')
            .setImage('https://picfiles.alphacoders.com/482/482212.png')
            .setFooter('Prefix for this server is: /')
            .setColor('ORANGE')

        
        await channel.send({
            files: [{
                attachment: 'src/images/titan-banner.png',
                name: 'titan-banner.png'
                }],
            })
        
        const queueMessage = await channel.send({embeds : [queueEmbed]})
        const nowPlayMessage = await channel.send({embeds : [nowPlayingEmbed]})
        
        for (const emoji of emojis) {
            nowPlayMessage.react(emoji)
        } 

        interaction.guild.db.setup = { channel_id : channel.id, queue_id : queueMessage.id, now_play_id : nowPlayMessage.id}
        interaction.guild.db.save().then(interaction.reply('Setup is loaded!'))
    }
}
