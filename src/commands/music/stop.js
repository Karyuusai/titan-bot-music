const Command = require('../../structures/Command')

module.exports = class extends Command {
    constructor(client) {
        super(client, {
            name: 'stop',
            description: 'Pula a música que está tocando.'
        })
    }

    run = (interaction) => {
        const player = this.client.manager.get(interaction.guild.id);
        if (!player) return interaction.reply("there is no player for this guild.");
    
        const { channel } = interaction.member.voice;
        
        if (!channel) return interaction.reply("you need to join a voice channel.");
        if (channel.id !== player.voiceChannel) return interaction.reply("you're not in the same voice channel.");
        
        player.destroy();
        return interaction.reply("destroyed the player.");
      }
}