const { Message } = require('discord.js')
const Command = require('../../structures/Command')


module.exports = class extends Command {
    constructor(client) {
        super(client, {
            name: 'play',
            description: 'Make the bot play music',
            options: [
                {
                    name: 'música',
                    description: 'Música',
                    type: 'STRING',
                    required: true
                }
            ]
        })
    }
    run = async (interaction) => {
        if (!interaction.member.voice.channel) return interaction.reply({ content: 'canal de voz', ephemeral: true})

        if(interaction.guild.me.voice.channel && interaction.guild.me.voice.channel.id !== interaction.member.voice.channel.id) return interaction.reply({ content: 'Mesmo canal', ephemeral: true})
        
        const search = interaction.options.getString('música')
        
        let res;

        try {
            res = await this.client.manager.search(search, interaction.user)

            if (res.loadType === 'LOAD_FAILED') throw res.exception
            else if (res.loadType === 'PLAYLIST_LOADED') throw { message: 'Sem playlist' }
        } catch (err) {
            return interaction.reply({ content: 'Deu ruim' , ephemeral: true })
        }

        if (!res?.tracks?.[0]) return interaction.reply({ content: 'Sem música', ephemeral: true })
        
        const player = this.client.manager.create({
            guild: interaction.guild.id,
            voiceChannel: interaction.member.voice.channel.id,
            textChannel: interaction.channel.id
        })

        player.connect()
        player.queue.add(res.tracks[0])

        if (!player.playing && !player.paused) player.play()

        return interaction.reply({ content: `${res.tracks[0]} na fila.`})
    }
}
