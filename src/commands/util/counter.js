const { MessageActionRow, MessageButton } = require('discord.js')
const Command = require('../../structures/Command')

const actionRow = new MessageActionRow()
    .addComponents(
        [
            new MessageButton()
                .setStyle('DANGER')
                .setLabel('-1')
                .setCustomId('REMOVE'),
            
            new MessageButton()
                .setStyle('SUCCESS')
                .setLabel('+1')
                .setCustomId('ADD'),
            
            new MessageButton()
                .setStyle('PRIMARY')
                .setLabel('ZERAR')
                .setCustomId('ZERAR')
        ]
    )

module.exports = class extends Command {
    constructor(client) {
        super(client, {
            name: 'counter',
            description: 'Start a counter in channel'
        })
    }

    run = async (interaction) => {
        let counter = 0

        const reply = await interaction.reply({
            content:`Contagem: \`${counter}\``,
            components: [actionRow],
            fetchReply: true 
        })

        const filter = (b) => b.user.id === interaction.user.id
        const collector = reply.createMessageComponentCollector({ filter, time: (15000) })

        collector.on('collect', (i) => {
            switch (i.customId) {
                case 'REMOVE':
                    counter--
                    break;
                case 'ADD':
                    counter++
                    break
                case 'ZERAR':
                    counter = 0
                    break
            }

            i.update({
                content:`Contagem: \`${counter}\``
            })
        })

        collector.on('end', (collected, reason) => {
            if (reason === 'time') interaction.editReply({
                content: `Counter finishid in \`${counter}\``,
                components: []
            })
        })
    }
}