const { Manager } = require('erela.js')
const NowPlayingEmbedChanger = require('../util/NowPlayingEmbedChanger')
const Queue = require('../util/queue/queue')

module.exports = (client) => {
    const queue = new Queue(client)
    return new Manager({
        nodes: [
            {
                host: "localhost",
                password: "youshallnotpass",
                port: 2333
            }
        ],

        autoPlay: true,
        send: (id, payload) => {
            const guild = client.guilds.cache.get(id)
            if (guild) guild.shard.send(payload)
        }
    })
    .on("nodeConnect", node => console.log(`Node "${node.options.identifier}" connected.`))
    .on("nodeError", (node, error) => console.log(
        `Node "${node.options.identifier}" encountered an error: ${error.message}.`
    ))
    .on("trackStart", (player, track) => {
        const nowPlayingEmbedChanger = new NowPlayingEmbedChanger(client, player.guild)
        const thumb = track.thumbnail.replace('default', 'maxresdefault')

        nowPlayingEmbedChanger.changeEmbed(track.title, thumb)
    })
    .on("trackEnd", (player, track) => {
        if (player.queue.size <= 0 && player.queue.current) return queue.clearQueue(player)
        if (player.queue.size <= 0) return

        return queue.changeQueue(player)
    })
    .on("queueEnd", player => {
        player.destroy()

    })
    .on("playerMove", (player, oldChannel, newChannel) => {
        if (newChannel) {
            player.voiceChannel = newChannel;
            setTimeout(() => {
                player.pause(false)
            }, 1000)
        } else {
            player.destroy()
        }
    })
    .on("playerDestroy", player => {
        const nowPlayingEmbedChanger = new NowPlayingEmbedChanger(client, player.guild)
        nowPlayingEmbedChanger.changeEmbed()

        queue.clearQueue(player)
    })
}