const { MessageEmbed } = require("discord.js")

module.exports = class NowPlayingEmbedChanger {
    constructor(client, guild_id) {
        this.client = client,
        this.guild = guild_id
    }

    changeEmbed = async(title = 'No song playing currently', image = 'https://picfiles.alphacoders.com/482/482212.png') => {
        const embed = new MessageEmbed()
            .setTitle(title)
            .setImage(image)
            .setColor('ORANGE')

        const db = await this.client.db.guilds.findById(this.guild)
        
        const message = await this.client.channels.cache.get(`${db.setup.channel_id}`).messages.fetch(`${db.setup.now_play_id}`)
       
        try {
            await message.edit({embeds:[embed]})

        }catch{ () => {
            
            }
        }
    }
}