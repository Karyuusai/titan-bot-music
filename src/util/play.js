const Queue = require('../util/queue/queue')

module.exports = class Play {
    constructor(client, message) {
        this.client = client,
        this.message = message
    }

    run = async() => {
            
            const player = this.client.manager.create({
                guild: this.message.guild.id,
                voiceChannel: this.message.member.voice.channel.id,
                textChannel: this.message.channel.id
            })
            
            const search = this.message.content
            let res;
            
            try {
                res = await this.client.manager.search(search, this.message.author)
 
                if (res.loadType === 'LOAD_FAILED') throw res.exception
                if (res.loadType === 'NO_MATCHES') return //this.message.reply({ content: 'Sem resultados', ephemeral:true })
                
                if (player.state !== "CONNECTED") player.connect();
                
                if (res.loadType === 'PLAYLIST_LOADED') {

                    if (res.tracks[search.split("index=")[1]]) {
                        player.queue.add(res.tracks.slice(search.split("index=")[1] - 1,))
                    }else{
                        player.queue.add(res.tracks)
                    }
                }
                else if (res.loadType === 'SEARCH_RESULT') {
                    player.queue.add(res.tracks[0])
                }
            } catch (err) {
                return
            }

            const queue = new Queue(this.client)
            queue.changeQueue(player)

            if (!player.playing && !player.paused) player.play()

        }
    }

