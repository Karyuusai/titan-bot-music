const { MessageEmbed } = require("discord.js")

module.exports = class Queue {
    constructor(client) {
        this.client = client
    }

    changeQueue = async(player) => {
 
        if (player.queue.size <= 0) return 

        const db = await this.client.db.guilds.findById(player.guild)
        const message = await this.client.channels.cache.get(`${db.setup.channel_id}`).messages.fetch(`${db.setup.queue_id}`)

        const maxItensPage = 5
        const pages = Math.ceil(player.queue.size / maxItensPage)

        const tracks = player.queue.slice(0, maxItensPage)
        
        const tracksMapped =  tracks.map((t, i) => {
            return `\`${i + 1}\.\` [${t.title}](${t.uri}) - **${t.requester.tag}**\n`
        })
        .join('\n')

        const next = player.queue.size > 0 ? `\n\n\n⬇️__Next Songs__⬇️\n\n` : '\n'
        const tracksInQueue = `\n\n**${player.queue.size} tracks in queue**`

        const footerData = {
            text:`Viewing page 1/${pages}`,
            iconURL: this.client.user.avatarURL()
        }

        const queueEmbed = new MessageEmbed()
            .setTitle('Queue')
            .setDescription(`${next}${tracksMapped}${tracksInQueue}`)    
            .setColor('ORANGE')
            //.setFooter(`Viewing page 1/${pages}`, this.client.user.avatarURL())
            .setFooter(footerData)
        
        await message.edit({ embeds: [queueEmbed] })
    }

    clearQueue = async(player) => {

        const db = await this.client.db.guilds.findById(player.guild)
        const message = await this.client.channels.cache.get(`${db.setup.channel_id}`).messages.fetch(`${db.setup.queue_id}`)
        
        const queueEmbed = new MessageEmbed()
            .setTitle('__Queue list__')
            .setDescription('Join a voice channel and queue songs by name or url in here.')
            .setColor('ORANGE')

        await message.edit({ embeds: [queueEmbed] })
    }
        
}